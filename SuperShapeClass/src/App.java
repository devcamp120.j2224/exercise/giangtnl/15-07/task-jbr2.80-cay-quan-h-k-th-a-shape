import javax.security.auth.login.CredentialException;

public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green" , false);

        System.out.println("shape1 : " + shape1.toString() + "\n" + 
                            " , shape 2 : " + shape2.toString());

        Circle circle1 = new Circle() ; 
        Circle circle2 = new Circle(2.0) ; 
        Circle circle3 = new Circle(3.0 , "green" , true) ; 


        System.out.println( "circle1 : " + circle1.toString() + "\n" + 
                            " , cirle2 : " + circle2.toString() + "\n" + 
                            " , circle3 : " +  circle3.toString());

        System.out.println(" , diện tích hình tròn circle1 : " + circle1.getArea() + "\n" + 
                            " , chu vi hình tròn circle1 : " + circle1.getPerimeter() + "\n" +
                            " , diện tích hình tròn circle2 : " + circle2.getArea() + "\n" + 
                            " , chu vi hình tròn circle2 : " + circle2.getPerimeter() + "\n" +
                            " , diện tích hình tròn circle3 : " + circle3.getArea() + "\n" + 
                            " , chu vi hình tròn circle3 : " + circle3.getPerimeter());                    

        Rectangle rectangle1 = new Rectangle();   
        Rectangle rectangle2 = new Rectangle(2.5 , 1.5);       
        Rectangle rectangle3 = new Rectangle(2.0 , 1.5 , "green" , true);         
        
        System.out.println( "rectangle1 : " + rectangle1.toString() + "\n" + 
                            " , rectangle2 : " + rectangle2.toString() + "\n" + 
                            " , rectangle3 : " +  rectangle3.toString());


        System.out.println(" , diện tích hình chữ nhật rectangle1 : " + rectangle1.getArea() + "\n" + 
        " , chu vi hình chữ nhật rectangle1 : " + rectangle1.getPerimeter() + "\n" +
        " , diện tích hình chữ nhật rectangle2 : " + rectangle2.getArea() + "\n" + 
        " , chu vi hình chữ nhật rectangle2 : " + rectangle2.getPerimeter() + "\n" +
        " , diện tích hình chữ nhật rectangle3 : " + rectangle3.getArea() + "\n" + 
        " , chu vi hình tchữ nhật rectangle3 : " + rectangle3.getPerimeter());          
        
        
        Square square1 = new Square();     
        Square square2 = new Square(1.5);        
        Square square3  = new Square(2.0 , "green" , true);       
        
        
        System.out.println( "square1 : " + square1.toString() + "\n" + 
                            " , square2 : " + square2.toString() + "\n" + 
                            " , square3 : " +  square3.toString());


        System.out.println(" , diện tích hình vuông square1 : " + square1.getArea() + "\n" + 
        " , chu vi hình vuông square1 : " + square1.getPerimeter() + "\n" +
        " , diện tích hình vuông square2 : " + square2.getArea() + "\n" + 
        " , chu vi hình vuông square2 : " + square2.getPerimeter() + "\n" +
        " , diện tích hình vuông square3 : " + square3.getArea() + "\n" + 
        " , chu vi hình vuông square3 : " + square3.getPerimeter());          
    }
}
